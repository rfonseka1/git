<!DOCTYPE html>
 
<html>
<head>
 
<script src="http://maps.googleapis.com/maps/api/js"></script>
<script>
 
/* consulto los datos a google con la latitud y longitud */
 
var localizacion=new google.maps.LatLng(40.4503252,-3.6929834);
 
 
/* función que configura e incializa el mapa */
 
 
function iniciar()
 
{
var mapa = {
 
  center:localizacion,
  zoom:18,
 
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };
 
 
var mapa=new google.maps.Map(document.getElementById("mapa"),mapa);
 
 
/* configura el icono puntero y le añado animación
 
var configicono=new google.maps.Marker({
  position:localizacion,
 
  animation:google.maps.Animation.BOUNCE
  });
 
 
configicono.setMap(mapa);
 
 
/* en info añado la informacion nombre de cliente o empresa en html */
 
var info = new google.maps.InfoWindow({
  content:"<strong>Google Madrid </strong><br> Teléfono: +34 917486400"
 
  });
 
info.open(mapa,configicono);
 
}
 
google.maps.event.addDomListener(window, 'load', iniciar);
</script>
 
</head>
 
<body>
<div id="mapa" style="width:500px;height:380px;"></div>
 
</body>
</html>