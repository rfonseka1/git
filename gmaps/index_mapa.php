<!DOCTYPE html>
 
<html lang="es">
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PRINCIPAL | INICIO</title>

    <!-- Bootstrap -->
    <link rel="icon" href="img/default_avatar.jpg">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap.css" rel="stylesheet">
    <link href="css/dashboard.css" rel="stylesheet">
    <link href="css/carusel.css" rel="stylesheet">
    <link href="css/dashboard.css" rel="stylesheet">
    <link href="css/validacion.css" rel="stylesheet">

      <script src="https://maps.googleapis.com/maps/api/js"></script>
    <link rel="stylesheet" type="text/css" href="css/estilos.css">
 
</head>
<body  background="img/fondo.gif">

<div class="container">
   <div class="well">
   <div class="row">
     <div class="col-sm-4 col-md-4">
          <strong>Mapa según Dirección</strong><br>
 
            <!-- Datos a buscar -->
            <form action="" method="post">

               <input  type="text" class="form-control" name="direccion" value="" placeholder="Dirección "><br>
               <input  type="text" class="form-control" name="ciudad" value="" placeholder="Ciudad "><br>
               <input  type="text" class="form-control" name="provincia" value="" placeholder="Provinvia "><br>
               <input  type="text" class="form-control" name="pais" value="" placeholder="País "><br>
             
              <input  type="submit" class="btn btn-success btn-lg btn-block"  value='Buscar' ">

               
                
            </form>

     </div>
     <div class="col-sm-6 col-md-6">
         <center><img src="img/buscar.jpg"  height=""  width=""></center>
     </div>
     <div class="col-sm-2 col-md-2"></div>
   </div>
   </div>
</div>   

 

 
<?php include("funciones.php"); ?>
<?php
 
if($_POST){
 
    // Buscamos la latitud, longitud en base a la direccion calle y número, ciudad, país
    $localizar=$_POST['direccion'].", ".$_POST['ciudad'].", ".$_POST['provincia'].", ".$_POST['pais'];
 
     
    $datosmapa = geolocalizar($localizar);
 
 
echo "<br><br><strong></stroing>Consulta: </strong>".$localizar;
 
    // Tomamos los datos que encontro la funcion
    if($datosmapa){
 
        
        $latitud = $datosmapa[0];
 
        $longitud = $datosmapa[1];
        $localizacion = $datosmapa[2];
 
}
}
 
        ?>
        
 
<div id="mapa" ></div>
 
   <script type="text/javascript">
        function init_map() {
 
            var myOptions = {
                zoom: 18,
 
                center: new google.maps.LatLng(<?php echo $latitud; ?>, <?php echo $longitud; ?>),
                mapTypeId: google.maps.MapTypeId.ROADMAP
 
            };
            map = new google.maps.Map(document.getElementById("mapa"), myOptions);
 
            marker = new google.maps.Marker({
                map: map,
 
                position: new google.maps.LatLng(<?php echo $latitud; ?>, <?php echo $longitud; ?>)
            });
 
            infowindow = new google.maps.InfoWindow({
                content: "<?php echo $localizacion; ?>"
 
            });
            google.maps.event.addListener(marker, "click", function () {
 
                infowindow.open(map, marker);
            });
 
            infowindow.open(map, marker);
        }
 
        google.maps.event.addDomListener(window, 'load', init_map);
    </script>
 
</body>
</html>