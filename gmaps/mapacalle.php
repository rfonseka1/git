<!DOCTYPE html>
<html>
 
  <head>
    <meta charset="utf-8">
 
    <style>
       #mapa {
 
        height: 100%;
        margin: 0;
 
        padding: 0;
      }
 
 
    </style>
 
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
    <script>
 
function iniciar() {
  var localizar = new google.maps.LatLng(40.43753,-3.623937);
 
  var mapaconfig = {
    center: localizar,
 
    zoom: 18
  };
 
  var mapa = new google.maps.Map(
      document.getElementById('mapa'), mapaconfig);
 
  var vistaconfig = {
    position: localizar,
 
    pov: {
      heading: 20,
 
      pitch: 5
    }
 
  };
  var vistacalle = new google.maps.StreetViewPanorama(document.getElementById('vista'), vistaconfig);
 
  mapa.setStreetView(vistacalle);
}
 
 
google.maps.event.addDomListener(window, 'load', iniciar);
 
 
    </script>
 
  </head>
  <body>
 
    <div id="mapa" style="width: 50%; height: 100%;float:left"></div>
    <div id="vista" style="width: 50%; height: 100%;float:left"></div>
 
  </body>
</html>